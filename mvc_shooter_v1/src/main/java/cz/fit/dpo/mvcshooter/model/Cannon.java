package cz.fit.dpo.mvcshooter.model;

/**
 *
 * @author Ondrej Stuchlik
 */
public class Cannon {

    private int x = 100;
    private int y = 100;

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
    
}
