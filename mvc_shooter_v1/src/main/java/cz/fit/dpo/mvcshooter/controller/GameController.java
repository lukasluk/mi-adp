package cz.fit.dpo.mvcshooter.controller;

import cz.fit.dpo.mvcshooter.model.GameModel;
import cz.fit.dpo.mvcshooter.view.Canvas;

import java.awt.event.KeyEvent;

public class GameController {

    private GameModel model;
    private Canvas view;

    public GameController(GameModel model) {
        this.model = model;
    }

    public void setView(Canvas view) {
        this.view = view;
    }

    public GameModel getModel() {
        return this.model;
    }

    public void onKeyPress(KeyEvent evt) {
        System.out.println("key pressed: " + evt.getKeyChar() + " (code: " + evt.getKeyCode() + ")" );

        switch (evt.getKeyCode()) {
            case KeyEvent.VK_UP:
                model.moveCannonUp();
            case KeyEvent.VK_DOWN:
                model.moveCannonDown();
            default:
                //nothing
        }

        view.thisIsHowYouForceGuiToRepaint();
    }
}
