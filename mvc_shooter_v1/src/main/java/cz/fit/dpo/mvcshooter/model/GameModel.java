package cz.fit.dpo.mvcshooter.model;

public class GameModel {

    private int moveStep = 10;
    private int score = 0;
    private Cannon cannon = new Cannon();

    public GameModel() {

    }

    public void moveCannonUp() {
        int y = cannon.getY();
        y = y - this.moveStep;
        this.cannon.setY(y);
    }

    public void moveCannonDown() {
        int y = cannon.getY();
        y += this.moveStep;
        this.cannon.setY(y);
    }

    public Cannon getCannon() {
        return this.cannon;
    }
}
